#!/usr/bin/python3
v='0.1'
import sys
sys.path.insert(0,'classes/')
from Server import Server
from config import lt_host, lt_port
from Debug import Debug
Debug.add('LightTracker, version '+v+'.','info')
S = Server(lt_host, lt_port)
if not S.bind():
  sys.exit(1)

S.serve()
