## LightTracker

### Présentation

*LightTracker* est un tracker UDP BitTorrent écrit en Python3.
Il n'a pas besoin d'utiliser de base de données sql ou sqlite : tous se
fait *en direct*.  
*LightTracker* est fait pour être ouvert et public : quiconque peut enregistrer un torrent simplement en ajouter l'URI du *LightTracker* dans la `announce-list`.

### Installation

Pour utiliser *LightTracker* sur Linux (Debian, Ubuntu ou autre) :
```bash
sudo aptitude install python3 python3-dev python3-pip
pip3 install iptools
git clone 'https://git.framasoft.org/zadig/LightTracker.git'
cd LightTracker
python3 __main__.py
```

### Documentation
Par défaut, *LightTracker* écoute sur toutes les interfaces, port 10425.
Pour changer ça, éditez les variables `lt_host` et `lt_port` dans le fichier `config.py`.

D'après les spécifications du protocole BitTorrent, la variable `action` peut recevoir 4 valeurs :
 - 0: connect
 - 1: announce
 - 2: scrape
 - 3: error  


(référence : http://bittorrent.org/beps/bep_0015.html)  
*LightTracker* embarque une cinquième valeur : *42*.
Cette valeur renvoie au format JSON le dictionnaire `Server.torrents` (au préalable passé à `Client`).  
Elle permet d'obtenir des statistiques sur l'usage du tracker.  

### Exemple  
Vous pouvez tester LightTracker en ajoutant à vos torrents le tracker suivant :  
```
udp://tracker.botch.io:10425
```