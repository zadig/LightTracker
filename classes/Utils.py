import Bencode
import binascii,urllib, codecs, re
from iptools.ipv4 import long2ip, ip2long
import json

class Utils:

  @staticmethod
  def bytes2hex(data):
    return codecs.getencoder('hex_codec')(data)[0].decode('utf-8')

  @staticmethod
  def hex2bytes(hexa):
    return bytes.fromhex(hexa)

  @staticmethod
  def bytes2url(data):
    return urllib.parse.quote(data)

  @staticmethod
  def hex2url(hexa):
    return urllib.parse.quote(Utils.hex2bytes(hexa))

  @staticmethod
  def bencode(data):
    return Bencode.encode(data)

  @staticmethod
  def bdecode(data):
    return Bencode.decode(data)

  @staticmethod
  def addslashes(text):
    regex = re.compile(b'\\\\(\\\\|[0-7]{1,3}|x.[0-9a-f]?|[\'"abfnrt]|.|$)')
    def replace(m):
        b = m.group(1)
        if len(b) == 0:
            raise ValueError("Invalid character escape: '\\'.")
        i = b[0]
        if i == 120:
            v = int(b[1:], 16)
        elif 48 <= i <= 55:
            v = int(b, 8)
        elif i == 34: return b'"'
        elif i == 39: return b"'"
        elif i == 92: return b'\\'
        elif i == 97: return b'\a'
        elif i == 98: return b'\b'
        elif i == 102: return b'\f'
        elif i == 110: return b'\n'
        elif i == 114: return b'\r'
        elif i == 116: return b'\t'
        else:
            s = b.decode('ascii')
            raise UnicodeDecodeError(
                'stringescape', text, m.start(), m.end(), "Invalid escape: %r" % s
            )
        return bytes((v, ))
    result = regex.sub(replace, text)

  @staticmethod
  def jsondecode(data):
    try:
      array = json.loads(data)
      return array
    except:
      return None

  @staticmethod
  def jsonencode(data):
    return json.dumps(data)

  @staticmethod
  def uniqueList(l):
    tmp = []
    for i in l:
      if i not in tmp:
        tmp.append(i)
    return tmp

  @staticmethod
  def ip2long(ip):
    return ip2long(ip)

  @staticmethod
  def long2ip(ip):
    return long2ip(ip)
