from datetime import datetime as Time
from Sexy import Sexy

class Debug:

  @staticmethod
  def add(txt, type='raw', time = True):
    color = {'error':'bldred',
             'raw':'bldylw',
             'ok':'bldgrn',
             'info':'bldcyn'}[type]
    string = txt
    if time:
      now = Time.now()
      string = '['+':'.join([str(i) for i in [now.hour,now.minute,now.second,now.microsecond] ]) + '] ' + string
    Sexy.out(string, color)

