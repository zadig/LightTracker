import threading
from struct import pack, unpack
import random
from Debug import Debug
from Utils import Utils
import json
# Useful : http://bittorrent.org/beps/bep_0015.html

class Client(threading.Thread):

  def __init__(self, data, addr, socket, clients, torrents, interval):

    # init vars
    self.data     = data
    Debug.add('Packet length:'+str(len(data)),'info')
    self.addr     = addr
    self.socket   = socket
    self.clients  = clients
    self.torrents = torrents
    self.interval = interval

    # init thread
    threading.Thread.__init__(self)

    # we check if the client exists already
    if addr[0] in self.clients:

      # we check the connection_id.
      # if the connection_id == 4497486125440, then it's a new connection..
      c,a,t = self.__extractCat()
      if c == 4497486125440:
        self.clients[addr[0]] = {}
        self.valid         = True
      else:
        # we get back data
        self.transaction_id  = self.clients[addr[0]]['transaction_id']
        self.connection_id   = self.clients[addr[0]]['connection_id']
        self.valid           = self.connection_id == c

    # New client
    else:
      # Bad client
      if len(self.data) != 16:
        self.valid   = False
      else:
      # Good client
        self.clients[addr[0]] = {}
        self.valid   = True


#######################################################################
#######################################################################
#######################################################################


  def run(self):

    # invalid request
    if not self.valid:
      self.__displayError('Invalid connection.')
      Debug.add("Invalid connection : "+str(len(self.data)),'raw')
      return False


    # case of new client
    if self.clients[self.addr[0]] == {}:
      # 1. Receive packet
      return self.__handshake()



    # We get Action
    connection_id,action,transaction_id = self.__extractCat()

    # on vérifie la transaction_id si elle n'a pas changé
    if transaction_id != self.transaction_id:
      self.transaction_id  = transaction_id
      self.clients[self.addr[0]]['transaction_id'] = transaction_id

    # announce
    if action == 1:
      Debug.add('Announce detected','ok')
      self.__announce()

    # scrape
    elif action == 2:
      Debug.add('Scrape detected','ok')
      self.__scrape()

    # datas
    elif action == 42:
      Debug.add('Get stats', 'ok')
      self.__42()
      return True

    # ?
    else:
      self.transaction_id  = 1
      Debug.add('Unknown action : '+str(action),'error')
      self.__displayError('Unknown action.')
      return False


#######################################################################
#######################################################################
#######################################################################


  def __handshake(self):
    Debug.add('New client : '+str(self.addr),'info')

    # 2. Check whether the packet is at least 16 bytes.
    if len(self.data) != 16:
      Debug.add('Error from Client: invalid packet size.', 'error')
      self.__displayError('Invalid request length.')
      return False


    connection_id, action, transaction_id = self.__extractCat()

    # 3. Check the connection_id
    if connection_id != 4497486125440:
      Debug.add('Error from Client: invalid initial connection_id.', 'error')
      self.__displayError('Invalid original connection_id.')
      return False

    # 4. Check the action
    if action != 0:
      Debug.add('Error from Client: invalid action: '+str(action), 'error')
      self.__displayError('Invalid action: need to be 0 at the first time.')
      return False

    # 5. We store the client
    self.clients[self.addr[0]]['connection_id']  = random.randint(9999999999,9999999999999)
    self.clients[self.addr[0]]['transaction_id'] = transaction_id

    # 6. We answer
    self.socket.sendto(pack('!IIQ',0,transaction_id,self.clients[self.addr[0]]['connection_id']),self.addr)
    Debug.add('connection_id envoyé:'+str(self.clients[self.addr[0]]['connection_id']),'ok')
    Debug.add('Client received the answer.', 'ok')
    return True


#######################################################################
#######################################################################
#######################################################################


  def __displayError(self,error):
    strlen = str(len(error))
    if hasattr(self,'transaction_id'):
      ti = self.transaction_id
    else:
      ti = 0
    self.socket.sendto(pack('!II'+strlen+'s',3,ti,bytes(error,'utf-8')),self.addr)


#######################################################################
#######################################################################
#######################################################################


  def __announce(self):

    # 1. We check the size (=98)
    if len(self.data) != 98:
      Debug.add('Client said announce but packet size is invalid.','error')
      self.__displayError('Invalid request length.')
      return False

    try:
      info_hash,peer_id,downloaded,left,uploaded,event, ip, key, numwant, port  = unpack('!20s20sQQQIIIIH',self.data[16:])
    except:
      Debug.add('Client send good length packet, but it is malformed.','error')
      self.__displayError('Malformed packet.')
      return False

    # 2. we catch the event


    # None
    if event == 0:
      self.__sendClientsList(info_hash)
      return True

    # Completed
    if event in [1,2,3]:
      self.__saveClient(info_hash, downloaded, left, uploaded, event, ip, port)
      self.__sendClientsList(info_hash)

    print(self.torrents[info_hash])
    Debug.add('Success for announce.','ok')
    return True

#######################################################################
#######################################################################
#######################################################################


  def __scrape(self):
    connection_id, action, transaction_id = self.__extractCat()
    data    = self.data[16:]
    reponse = pack('!II', 2, transaction_id)
    for i in range(len(data)//20):
      info_hash = data[15+20*i:15+20*(i+1)]
      s = self.__stats(info_hash)
      reponse += pack('!III',s['seeders']+s['leechers'], s['seeders'], s['leechers'])
    self.socket.sendto(reponse,self.addr)
    return True


#######################################################################
#######################################################################
#######################################################################


  def __extractCat(self):
    return unpack('!QII',self.data[0:16])


#######################################################################
#######################################################################
#######################################################################


  def __sendClientsList(self, info_hash):
    s = self.__stats(info_hash)
    if info_hash in self.torrents:
      clients = []
      for c in self.torrents[info_hash]:
        if c['status'] != 3:
          clients.append([c['ip'],c['port']])

      reponse = pack('!IIIII', 1, self.transaction_id, self.interval, s['leechers'], s['seeders'])
      for c in clients:
        reponse += pack('!IH',Utils.ip2long(c[0]),c[1])
      self.socket.sendto(reponse, self.addr)
      return True
    else:
      return pack('!IIIII', 1, self.transaction_id, self.interval, 0, 0)


#######################################################################
#######################################################################
#######################################################################


  def __stats(self, info_hash):
    answer = {'seeders':0,'leechers':0, 'completed':0}
    if info_hash not in self.torrents:
      return answer

    for c in self.torrents[info_hash]:
      # event 'completed'
      if c['status'] == 1:
        answer['seeders'] += 1
      elif c['status'] == 2:
        answer['leechers'] += 1
    return answer


#######################################################################
#######################################################################
#######################################################################


  def __saveClient(self, info_hash, downloaded, left, uploaded, event, ip, port):
    if ip != 0:
      ip = Utils.long2ip(ip)
    else:
      ip = self.addr[0]
    if info_hash in self.torrents:
      for i in range(len(self.torrents[info_hash])):
        c = self.torrents[info_hash][i]
        if c['ip'] == ip:
          c['status'] = event
          return True
      self.torrents[info_hash].append({'ip':ip,'port':port,'downloaded':downloaded,'left':left,'uploaded':uploaded,'status':event})
    else:
      self.torrents[info_hash] = []
      self.torrents[info_hash].append({'ip':ip,'port':port,'downloaded':downloaded,'left':left,'uploaded':uploaded,'status':event})
    return True


#######################################################################
#######################################################################
#######################################################################


  def __42(self):
    torrents = {}
    for t in self.torrents:
      torrents[Utils.bytes2hex(t)] = self.torrents[t]

    j = json.dumps(torrents).encode('utf-8')
    s = 0
    while s < len(j)-1:
      s += self.socket.sendto(j[s:], self.addr)
    return True
