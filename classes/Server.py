import socket
import sys
from Debug import Debug
from Utils import Utils
from struct import pack, unpack
from threading import Thread
from Client import Client

class Server:

  def __init__(self, ip, port, interval = 100):
    self.ip       = ip
    self.port     = int(port)
    self.torrents = {}
    self.clients  = {}
    self.interval = interval

  def bind(self):
    Debug.add('Binding on '+self.ip+':'+str(self.port)+'...','info')
    try:
      self.socket  = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
      self.socket.bind((self.ip,self.port))
    except OSError as e:
      Debug.add('Error while launching the server : '+e.args[1] + ' (' + str(e.args[0]) + ')','error')
      return False
    return True

  def serve(self):
    while True:
      data, addr  = self.socket.recvfrom(1024)
      c = Client(data, addr, self.socket, self.clients, self.torrents, self.interval)
      c.start()
