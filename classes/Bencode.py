def encode(etwas):
	if type(etwas) is str:
		return '%d:%s' % ( len(etwas), etwas )

	elif type(etwas) is int:
		return 'i%de' % etwas

	elif type(etwas) is list:
		leerer_string = ''

		for item in etwas:
			leerer_string += encode(item)

		return 'l%se' % leerer_string

	elif type(etwas) is dict:
		leerer_string = ''

		for schlüssel, wert in etwas.items():
			leerer_string += encode(str(schlüssel))
			leerer_string += encode(wert)

		return 'd%se' % leerer_string
	raise TypeError('Invalid Input')


import re

def decode(etwas):
	if type(etwas) == str:
		if re.match('^[[0-9]*:', etwas):
			item = etwas.split(':',1)
			länge = int(item[0])
			text = item[1]
			string_text = text[:länge]
			return string_text

		elif re.match('^i-?[0-9]*e',etwas):
			zahl = int(etwas.split('e',1)[0][1:])
			return  zahl

		elif etwas[0] == 'l' and etwas[-1] == 'e':
			inhalt = etwas[1:-1]
			liste = list()

			while inhalt != '' and inhalt[0] != 'e':
				ergebnis = decode(inhalt)
				liste.append(ergebnis)
				inhalt = inhalt[len(str(encode(ergebnis))):]
			return liste

		elif etwas[0] == 'd' and etwas[-1] == 'e':
			inhalt = etwas[1:-1]
			temp_liste = decode('l%se' % inhalt)
			dictio = dict()
			for item in temp_liste[::2]:
				if type(item) is str:
					listenposition = temp_liste.index(item)
					try:
						value = temp_liste[listenposition + 1]
					except IndexError:
						print('Item ' + item + 'has no Value')

				else:
					raise ValueError('Missing keystring for Value "%s"' % str(item))

				dictio[item] = value
			return dictio
		raise TypeError(etwas[0]+':'+etwas[-1]+' - Element is invalid: "%s"' % str(etwas))





import unittest
from collections import OrderedDict

class TestEncodingFunctions(unittest.TestCase):

	def test_strings_encoding(self):
		self.assertEqual('10:Hallo Welt',encode('Hallo Welt'))

	def test_int_encoding(self):
		self.assertEqual('i42e',encode(42))

	def test_int_negative_encoding(self):
		self.assertEqual('i-42e',encode(-42))

	def test_list_encoding(self):
		self.assertEqual('l5:Hallo4:Welte',encode(['Hallo','Welt']))

	def test_invalid_float_input(self):
		self.assertRaises(TypeError,encode,3.14)

	def test_dictionary_encoding(self):
		self.assertEqual('d3:fooi23ee', encode({'foo': 23}))

	def test_nested_list_encoding(self):
		self.assertEqual('lli2e5:halloed3:foo3:baree', encode([[2,'hallo'],{'foo':'bar'}]))

	def test_nested_dict_encoding(self):
		self.assertEqual('d5:listel5:halloi45ed3:foo3:bareee',encode({'liste':['hallo',45,{'foo':'bar'}]}))


class TestDecodingFunctions(unittest.TestCase):

	def test_strings_decoding(self):
		self.assertEqual('Hallo Welt',decode('10:Hallo Welt'))

	def test_int_decoding(self):
		self.assertEqual(42,decode('i42e'))

	def test_int_negative_decoding(self):
		self.assertEqual(-42,decode('i-42e'))

	def test_list_decoding(self):
		self.assertEqual(['Hallo','Welt'],decode('l5:Hallo4:Welte'))

	def test_dictionary_decoding(self):
		self.assertEqual({'foo': 23}, decode('d3:fooi23ee'))

	def test_nested_list_decoding(self):
		self.assertEqual([[2,'hallo'],{'foo':'bar'}], decode('lli2e5:halloed3:foo3:baree'))

	def test_nested_dict_decoding(self):
		self.assertEqual({'liste':['hallo',45,{'foo':'bar'}]},decode('d5:listel5:halloi45ed3:foo3:bareee'))


if __name__ == '__main__':
	unittest.main(verbosity=2)
